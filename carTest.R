library(ggplot2)
library(dplyr)
library(caTools)
library(lubridate)
library(tm)
library(ROSE)
library(corrplot)
library(rpart)
library(rpart.plot)
library(randomForest)
library(pROC)

#Data Set
str(mtcars)
cars <- mtcars
#============Q1============
#median of mpg
median.mpg<-median(cars$mpg)
#function -> change mpg value to yes and no 
mpg.YN <- function(mpg){
  if(mpg>median.mpg){
    return ('yes')
  } 
  else {
    return ('no')
  }
}

cars$mpg <- sapply(cars$mpg, mpg.YN)
summary(cars) #mpg = character
#change mpg to factor
cars$mpg <- as.factor(cars$mpg)
str(cars)
summary(cars)

#============Q2============
filter <- sample.split(cars$mpg , SplitRatio = 0.7)
cars.train <- subset(cars, filter ==T)
cars.test <- subset(cars, filter ==F)
dim(cars.train)
dim(cars.test)
dim(cars)

model.dt <- rpart(mpg~.,cars.train)
rpart.plot(model.dt, box.palette="RdBu", shadow.col="gray", nn=TRUE)


#============Q3============

#============Q4============
str(cars)
breaks <- c(0,6,8)
labels <- c("low", "high")
bins <- cut(cars$cyl,breaks, include.lowest = T, right = F,labels = labels)
cars$cylbin <- bins
cars$cylbin <- NULL











